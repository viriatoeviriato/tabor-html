var nifs = {
  "GB": [
    "^GB\\d{9}$",
    "^GBGD(00[0-9]|4[0-9][0-9])$",
    "^GBHA(5[0-9][0-9]|9[0-9][0-9])$",
    ""], //DONE
  "JE": ["", ""],
  "GG": ["", ""],
  "IM": ["", ""],
  "US": ["", ""],
  "CA": ["", ""],
  "DE": ["", ""],
  "JP": ["", ""],
  "FR": ["^FR[A-Z0-9]{2}\\d{9}$", "(FRXX#########)"], //DONE
  "AU": ["", ""],
  "IT": ["^\\d{11}$", "(###########)"], //DONE
  "CH": ["", ""],
  "AT": ["^AT[ ]U[a-zA-Z0-9]{8}$", "(AT U########)"], //DONE
  "ES": ["^ES[A-Z0-9]\\d{7}[A-Z0-9]$", "(ESX#######X)"], //DONE
  "NL": ["^NL\\d{9}B\\d{2}$", "(NL#########B##)"], //DONE
  "BE": ["^BE[ ]\\d{4}[ ]\\d{3}[ ]\\d{3}$", "(BE #### ### ###)"], //DONE
  "DK": ["^DK\\d{8}$", "(DK#######)"], //DONE
  "SE": ["^\\d{12}$", "(############)"], //DONE
  "NO": ["^NO[ ]\\d{3}[ ]\\d{3}[ ]\\d{3}$", "(NO ### ### ###)"],
  "BR": ["^\\d{2}.\\d{3}.\\d{3}/\\d{4}-\\d{2}$", "(##.###.###/####-##)"], //DONE
  "PT": ["^[1-3,5]\\d{8}$", "(#########)"], //DONE
  "FI": ["^FI\\d{8}$", "(FI########)"], //DONE
  "AX": ["", ""],
  "KR": ["", ""],
  "CN": ["", ""],
  "TW": ["", ""],
  "SG": ["", ""],
  "DZ": ["", ""],
  "AD": ["", ""],
  "AR": ["^\\d{2}-\\d{8}-\\d{1}$", "(##-########-#)"], //DONE
  "AM": ["", ""],
  "AZ": ["", ""],
  "BH": ["", ""],
  "BD": ["", ""],
  "BB": ["", ""],
  "BY": ["^УНП[ ]\\d{9}$", "(УНП #########)"], //DONE
  "BM": ["", ""],
  "BA": ["", ""],
  "IO": ["", ""],
  "BN": ["", ""],
  "BG": ["", ""],
  "KH": ["", ""],
  "CV": ["", ""],
  "CL": ["^\\d{8}-\\d{1}$", "(########-#)"], //DONE
  "CR": ["", ""],
  "HR": ["^HR\\d{11}$", "(HR###########)"], //DONE
  "CY": ["^CY[a-zA-Z0-9]{9}$", "(CY#########)"], //DONE
  "CZ": ["^CZ\\d{8,10}$", "(CZ########)"], //DONE
  "DO": ["", ""],
  "EC": ["^\\d{13}$", "(#############)"], //DONE
  "EG": ["", ""],
  "EE": ["^\\d{9}$", "(########)"], // DONE
  "FO": ["", ""],
  "GE": ["", ""],
  "GR": ["", ""],
  "GL": ["", ""],
  "GT": ["^\\d{7}-\\d{1}$", "(#######-#)"], //DONE
  "HT": ["", ""],
  "HN": ["", ""],
  "HU": ["^HU\\d{8}$", "(HU########)"], //DONE
  "IS": ["", ""],
  "IN": ["", ""],
  "ID": ["", ""],
  "IL": ["", ""],
  "JO": ["", ""],
  "KZ": ["", ""],
  "KE": ["", ""],
  "KW": ["", ""],
  "LA": ["", ""],
  "LV": ["^\\d{11}$", "(###########)"], //DONE
  "LB": ["", ""],
  "LI": ["", ""],
  "LT": ["^\\d{9,12}$", "(#########)"], //DONE
  "LU": ["^\\d{8}$", "(########)"], //DONE
  "MK": ["", ""],
  "MY": ["", ""],
  "MV": ["", ""],
  "MT": ["^\\d{8}$", "(########)"], //DONE
  "MU": ["", ""],
  "MX": ["", ""],
  "MD": ["", ""],
  "MC": ["", ""],
  "MA": ["", ""],
  "NP": ["", ""],
  "NZ": ["", ""],
  "NI": ["", ""],
  "NG": ["", ""],
  "OM": ["", ""],
  "PK": ["", ""],
  "PY": ["", ""],
  "PH": ["^\\d{3}-\\d{3}-\\d{3}-\\d{3}[ ]VAT$", "(###-###-###-### VAT)"], //DONE
  "PL": ["^\\d{10}$", "(##########)"], //DONE
  "PR": ["", ""],
  "RO": ["^\\d{2,10}$", ""], //DONE
  "RU": ["", ""],
  "SM": ["", ""],
  "SA": ["", ""],
  "SN": ["", ""],
  "SK": ["^SK[ ]\\d{10}$", "(SK ##########)"], //DONE
  "SI": ["^\\d{8}$", "(########)"], //DONE
  "ZA": ["", ""],
  "LK": ["", ""],
  "TJ": ["", ""],
  "TH": ["", ""],
  "TN": ["", ""],
  "TR": ["", ""],
  "TM": ["", ""],
  "UA": ["", ""],
  "UY": ["", ""],
  "UZ": ["", ""],
  "VA": ["", ""],
  "VE": ["", ""],
  "ZM": ["", ""],
  "AS": ["", ""],
  "CC": ["", ""],
  "CK": ["", ""],
  "RS": ["", ""],
  "ME": ["", ""],
  "CS": ["", ""],
  "YU": ["", ""],
  "CX": ["", ""],
  "ET": ["", ""],
  "FK": ["", ""],
  "NF": ["", ""],
  "FM": ["", ""],
  "GF": ["", ""],
  "GN": ["", ""],
  "GP": ["", ""],
  "GS": ["", ""],
  "GU": ["", ""],
  "GW": ["", ""],
  "HM": ["", ""],
  "IQ": ["", ""],
  "KG": ["", ""],
  "LR": ["", ""],
  "LS": ["", ""],
  "MG": ["", ""],
  "MH": ["", ""],
  "MN": ["", ""],
  "MP": ["", ""],
  "MQ": ["", ""],
  "NC": ["", ""],
  "NE": ["", ""],
  "VI": ["", ""],
  "PF": ["", ""],
  "PG": ["", ""],
  "PM": ["", ""],
  "PN": ["", ""],
  "PW": ["", ""],
  "RE": ["", ""],
  "SH": ["", ""],
  "SJ": ["", ""],
  "SO": ["", ""],
  "SZ": ["", ""],
  "TC": ["", ""],
  "WF": ["", ""],
  "XK": ["", ""],
  "YT": ["", ""]
};

(function(){
  document.addEventListener("DOMContentLoaded", function(event) {

    var countryIso = null;
    var region = null;
    var order = null;

    function validateRecuit(form){
      //console.log('Validate form');
      var valid = true;
      var oldPass = "";
      var pass = "";
      var confirmPass = "";
      var codeRegex = "";
      var nifRegex = "";

      $('.required',$(form)).each(function(){
        //console.log('input',$(this).val());
        if($(this).hasClass('country')){
          if($(this).val() == ""){
            $('a.country').addClass('error');
            valid = false;
          }else{
            $('a.country').removeClass('error');
          }
        }else if($(this).hasClass('subject')){
          if($(this).val() == ""){
            $('a.subject').addClass('error');
            valid = false;
          }else{
            $('a.subject').removeClass('error');
          }
        }else if($(this).hasClass('region')){
          if(region !== null && region == true){
            codeRegex = RegExp('^US-[A-Z]{2}$');
            if(!codeRegex.test($(this).val())){
              $(this).addClass('error');
              valid = false;
            }else{
              $(this).removeClass('error');
            }
          }
        }else if($(this).hasClass('order')){
          if(order !== null && order == true){
            if($(this).val() == ""){
              $(this).addClass('error');
              valid = false;
            }else{
              $(this).removeClass('error');
            }
          }
        }else if($(this).hasClass('code')){
          if(countryIso !== null){
            codeRegex = RegExp(zipcodes[countryIso][0]);
            if(!codeRegex.test($(this).val())){
              $(this).addClass('error');
              valid = false;
            }else{
              $(this).removeClass('error');
            }
          }else{
            $(this).addClass('error');
            valid = false;
          }
        }else if($(this).hasClass('nif')){
          if(countryIso !== null){
            if(countryIso=="GB")
            {
              if($(this).val().indexOf('GBGD')!=-1)
              {
                nifRegex = RegExp(nifs[countryIso][1]);

              }
              else if($(this).val().indexOf('GBHA')!=-1){
                nifRegex = RegExp(nifs[countryIso][2]);

              }
              else
              {
                nifRegex = RegExp(nifs[countryIso][0]);

              }
            }
            else
              nifRegex = RegExp(nifs[countryIso][0]);

            if(!nifRegex.test($(this).val())){
              $(this).addClass('error');
              valid = false;
            }else{
              $(this).removeClass('error');
            }
          }else{
            $(this).addClass('error');
            valid = false;
          }
        }else{
          if(!$(this).hasClass('password')){

            if($(this).val() == ""){
              $(this).addClass('error');
              valid = false;
            }else{
              if($(this).attr('type') == 'checkbox'){
                if($(this).prop('checked')){
                  $(this).parent().removeClass('error');
                }else{
                  $(this).parent().addClass('error');
                  valid = false;
                }
              }else{
                $(this).removeClass('error');
              }
            }

          }
        }
      });

      $('.required.password',$(form)).each(function(){
        //console.log('input',$(this).val());
        if($(this).hasClass('old')){
          oldPass = $(this).val();
        }else if($(this).hasClass('conf')){
          confirmPass = $(this).val();
        }else if($(this).hasClass('nova')){
          pass = $(this).val();
        }
      });

      if($('.required.password.old',$(form)).val() !== undefined && $('.required.password.old',$(form)).val() !== null ){
        console.log('here');
        $('.required.password',$(form)).removeClass('error');
        if(oldPass !== "" || pass !== "" || confirmPass !== "" ) {

          if(valid && (oldPass == "" && pass !== "") || (oldPass == "" && confirmPass !== "")) {
            valid = false;
            $('.required.password.old',$(form)).addClass('error');
          }

          if(valid && (oldPass !== "" && pass !== "") || (oldPass !== "" && confirmPass !== "")) {

            $('.required.password',$(form)).removeClass('error');

            if(pass !== confirmPass) {
              valid = false;
              $('.required.password.nova', $(form)).addClass('error');
              $('.required.password.conf', $(form)).addClass('error');
            }
          }else if(valid && pass !== "" || confirmPass !== ""){
            $('.required.password',$(form)).removeClass('error');

            if(pass !== confirmPass) {
              valid = false;
              $('.required.password.nova', $(form)).addClass('error');
              $('.required.password.conf', $(form)).addClass('error');
            }

          }

        }else if(oldPass == ""){
          valid = false;
          $('.required.password.old',$(form)).addClass('error');
        }
      }else if($('.required.password.conf',$(form)).val() !== undefined && $('.required.password.conf',$(form)).val() !== null ) {
        if (($('.required.password.nova', form).val() !== $('.required.password.conf', form).val()) || ($('.required.password.nova', form).val() == "" || $('.required.password.conf', form).val() == "")){
          valid = false;
          $('.required.password.nova', form).addClass('error');
          $('.required.password.conf', form).addClass('error');
        } else {
          if($('.required.password.nova', form).val() !== ""  && $('.required.password.conf', form).val() !== "" ){
            $('.required.password.nova', form).removeClass('error');
            $('.required.password.conf', form).removeClass('error');
          }
        }
      }

      return valid;
    }

    window.mobilecheck = function() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
    }

    if (!Array.prototype.indexOf) {
      Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
        'use strict';
        if (this == null) {
          throw new TypeError();
        }
        var n, k, t = Object(this),
          len = t.length >>> 0;

        if (len === 0) {
          return -1;
        }
        n = 0;
        if (arguments.length > 1) {
          n = Number(arguments[1]);
          if (n != n) { // shortcut for verifying if it's NaN
            n = 0;
          } else if (n != 0 && n != Infinity && n != -Infinity) {
            n = (n > 0 || -1) * Math.floor(Math.abs(n));
          }
        }
        if (n >= len) {
          return -1;
        }
        for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
          if (k in t && t[k] === searchElement) {
            return k;
          }
        }
        return -1;
      };
    }

    window.mobileAndTabletcheck = function() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
    }

    //$('#viriato').SignaturePlugin({type:1,color:'white',alignment:'center'});

    var dataImgs = "imgs";
    var size = 1;

    if(window.mobileAndTabletcheck()){
      dataImgs = "imgs-smaller";
      size = 2;
    }

    $(window).load(function () {
      //$('html').addClass('loaded');
      setTimeout(function(){ centerItems(); }, 500);
      setTimeout(function(){ $('div.top div.new').hide(); }, 3000);
    });

    function scrollTO(elem){
      //console.log('here scroll',elem);
      var heightTop = 60;
      if($('body').hasClass('saddledetail') || $('body').hasClass('produtos')){
        heightTop = 100;
      }
      $('html, body').animate({
        scrollTop: $(elem).offset().top-heightTop
      }, 500);
    }

    $('a.top').on('click touch',function(){
      scrollTO('body');
    });

    $('#newsletterForm input').focus(function(){
      if($(this).val() == $(this).data('text')){
        $(this).val('');
      }
    });

    $('#newsletterForm input').blur(function(){
      if($(this).val() == ''){
        $(this).val($(this).data('text'));
      }
    });

    /*$('#newsletterForm').submit(function(e){
      if(!validateRecuit(this)){
        e.preventDefault();
      }
    });*/
    $('a.lang').on('click touch',function(){
      if($(this).hasClass('active')){
        $('div.top ul.languages').hide();
        $(this).removeClass('active');
      }else{
        $('div.top ul.languages').show();
        $(this).addClass('active');
      }
    });

    $('div.top a.login').on('click touch',function(){
      if($(this).hasClass('active')){
        $('div.top form#loginForm').hide();
        $('div.top form#recoverForm').hide();
        $(this).removeClass('active');
      }else{
        $('div.top form#loginForm').show();
        $(this).addClass('active');
      }
    });

    $('div.top a.logout').on('click touch',function(){
      $('div.top form#logoutForm').submit();
    });

    $('div.top a.recover').on('click touch',function(){
        $('div.top form#loginForm').hide();
        $('div.top form#recoverForm').show();
    });

    $('div.top a.voltar').on('click touch',function(){
        $('div.top form#loginForm').show();
        $('div.top form#recoverForm').hide();
    });

    if($.cookie('bolachatabor')){
      $('#bolacha').hide();
    }
    $('#bolacha a').click(function(){
      $('#bolacha').hide();
      $.cookie('bolachatabor', 'true');
    });


    $("#loginForm").submit(function(e){
      if(validateRecuit("#loginForm")){
        return true;
      }
      e.preventDefault();
    });

    $("#recoverForm").submit(function(e){
      if(validateRecuit("#recoverForm")){
        return true;
      }
      e.preventDefault();
    });

    if($('body').hasClass('terms')) {

      if($('body').hasClass('faqs')) {

        $('ul.questions a').on('click touch',function(){
          if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).next().slideUp(200);
          }else{
            $('ul.questions a.active').next().slideUp(200);
            $('ul.questions a.active').removeClass('active');
            $(this).addClass('active');
            $(this).next().slideDown(200);
          }
        });

      }else if(!$('body').hasClass('contact')) {

        $('ul a').on('click touch',function(){
          scrollTO('#'+$(this).data('goto'));
        });

      }

    }

    if($('body').hasClass('register')){

      if(!$('body').hasClass('contact')){

        countryIso = $('label.code').data('iso');
        if(countryIso !== null && countryIso !== undefined){
          countryIso = countryIso.toUpperCase();
        }
        region = $('div.input.region').data('code');
        if(countryIso !== "" && countryIso !== null && countryIso !== undefined){
          $('label.code span').text(zipcodes[countryIso][zipcodes[countryIso].length-1]);
          $('label.nif span').text(nifs[countryIso][nifs[countryIso].length-1]);
        }
        if(region == true){
          $('div.input.region').removeClass('hidden');
        }

      if($('#userForm').hasClass('failed')){
        $('a.cancel, a.save, .edit').show();
        $('a.editprofile').hide();
        $('.static').hide();
      }

      $("#registerForm").submit(function(e){
        if(validateRecuit("#registerForm")){
          return true;
        }
        e.preventDefault();
      });

      $("#userForm").submit(function(e){
        if(validateRecuit("#userForm")){
          return true;
        }
        e.preventDefault();
      });

      $('a.country').click(function(){
        $('ul.countries').show();
      });

      $(' ul.countries a').click(function(){
        $(' ul.countries a').removeClass('active');
        $('ul.countries ').hide();
        $('a.country').text($(this).data('title'));
        $('input[name="country"]').val($(this).data('value'));
        countryIso = $(this).data('iso').toUpperCase();
        region = $(this).data('code');
        $('label.code span').text(zipcodes[countryIso][zipcodes[countryIso].length-1]);
        $('label.nif span').text(nifs[countryIso][nifs[countryIso].length-1]);
        if(region == true){
          $('div.region.input').removeClass('hidden');
        }else{
          $('div.region.input').addClass('hidden');
        }
        $(this).addClass('active');
      });

      $('div.checkbox').on('click touch',function(){
        if($(this).hasClass('active')){
          $("input",$(this)).prop( "checked", false );
          $(this).removeClass('active');
        }else{
          $("input",$(this)).prop( "checked", true );
          $(this).addClass('active');
        }
      });

      $('a.editprofile').on('click touch',function(){
        $('a.cancel, a.save, .edit').show();
        $(this).hide();
        $('.static').hide();
      });

      $('a.cancel').on('click touch',function(){
        $('a.cancel, a.save, .edit').hide();
        $('a.editprofile, .static').show();
        $('#userForm input').each(function(){
          $(this).val($(this).data('cancel'));
        });
        $('.error').removeClass('error');
      });

      $('a.save').on('click touch',function(){
        $("#userForm").submit();
      });

    }
    }

    if($('body').hasClass('contact')){

      order = $('div.input.order').data('order');
      /* = $('label.code').data('iso').toUpperCase();
      region = $('div.input.region').data('code');
      if(countryIso !== ""){
        $('label.code span').text(zipcodes[countryIso][zipcodes[countryIso].length-1]);
        $('label.nif span').text(nifs[countryIso][nifs[countryIso].length-1]);
      }*/
      if(order == true){
        $('div.input.order').removeClass('hidden');
      }

      $("#contactForm").submit(function(e){
        if(validateRecuit("#contactForm")){
          return true;
        }
        e.preventDefault();
      });

      $('a.subject').click(function(){
        $('ul.subjects').show();
      });

      $(' ul.subjects a').click(function(){
        $(' ul.subjects a').removeClass('active');
        $('ul.subjects ').hide();
        $('a.subject').html($(this).data('title')+'<span>v</span>');
        $('input[name="subject"]').val($(this).data('value'));
        order = $(this).data('order');

        if(order == true){
          $('div.order.input').removeClass('hidden');
        }else{
          $('div.order.input').addClass('hidden');
        }
        $(this).addClass('active');
      });

      $('div.checkbox').on('click touch',function(){
        if($(this).hasClass('active')){
          $("input",$(this)).prop( "checked", false );
          $(this).removeClass('active');
        }else{
          $("input",$(this)).prop( "checked", true );
          $(this).addClass('active');
        }
      });

    }

    function bottomText(){
      $('section.blog div.copy h4').css({'margin-top':0});
      var textHeight = $('section.blog div.copy h4').innerHeight();
      var copyHeight = $('section.blog div.copy').height();

      console.log('text, copy',textHeight,copyHeight);
      if(textHeight < copyHeight){
        $('section.blog div.copy h4').css({'margin-top':0});
        $('section.blog div.copy h4').css({'margin-top':copyHeight-textHeight});
      }else{
        $('section.blog div.copy h4').css({'margin-top':0});
      }
      $('.image').each(function(){
        $(this).backstretch("resize");
      });
    }

    if($('body').hasClass('homepage')){
      Hyphenator.config({
        'onafterwordhyphenation': function (hword, lang) {
          setTimeout(function(){ bottomText(); }, 500);
          return hword;
        }
      });
      Hyphenator.run();
      //$('div.copy.hyphenate h4').fitText();
      $(window).resize(function(){
        setTimeout(function(){ bottomText(); }, 500);
      });
      $('.image').each(function(){
        var img = $(this).data(dataImgs);
        $(this).backstretch(img, {fade:0}).backstretch("pause");
      });

    }
    if($('body').hasClass('ultra')){

      Hyphenator.run();

      $(window).resize(function(){

        $('.image').each(function(){
          $(this).backstretch("resize");
        });

      });

      $('.image').each(function(){
        var img = $(this).data(dataImgs);
        $(this).backstretch(img, {fade:0}).backstretch("pause");
      });

    }
    if($('body').hasClass('ultraview')){

      $(window).resize(function(){

        $('.image').each(function(){
          $(this).backstretch("resize");
        });

      });

      $('.image').each(function(){
        var img = $(this).data(dataImgs);
        $(this).backstretch(img, {fade:0}).backstretch("pause");
      });

    }

    if($('body').hasClass('tabor')){

      $('#wrapper').backstretch($('#wrapper').data(dataImgs), {fade:0}).backstretch("pause"); //,centeredX:false,centeredY:false

      $(window).resize(function(){

        $('.image').each(function(){
          $(this).backstretch("resize");
        });

        $('#wrapper').backstretch("resize");

      });

      $('.image').each(function(){
        var img = $(this).data(dataImgs);
        $(this).backstretch(img, {fade:0}).backstretch("pause");
      });

    }

    if($('body').hasClass('saddles')){

      //$('#wrapper').backstretch($('#wrapper').data(dataImgs), {fade:'fast',duration: 4000}).backstretch("pause");
      $("p.desc").dotdotdot({
        // configuration goes here
      });
      $(window).resize(function(){

        //$('.image').each(function(){
          //$(this).backstretch("resize");
        //});

       // $('#wrapper').backstretch("resize");

      });

      //$('.image').each(function(){
        //var img = $(this).data(dataImgs);
        //$(this).backstretch(img, {fade:'fast',duration: 4000}).backstretch("pause");
      //});

    }

    function centerItems(){
      $('div.block__one, div.block__two').each(function(){
        var textSize = $('h3',this).height();
        var imagesSize = $('div.images',this).height();
        var margin = 0;

        $('h3',this).css({'margin-top':0});
        $('div.images',this).css({'margin-top':0});
        if($(window).width() > 767){
          if(textSize >= imagesSize){
            $('div.images',this).css({'margin-top': (textSize-imagesSize)/2});
          }else{
            $('h3',this).css({'margin-top': (imagesSize-textSize)/2});
          }
        }
      });
    }

    if($('body').hasClass('saddledetail')){

      $('article.product__details .images').css({
        'margin-top': $('article.product__details h1').height()
      });
      $('.images').slick({
        dots: false,
        infinite: true,
        speed:0,
        fade: true,
        arrows: false,
        adaptiveHeight: true,
        touchMove: false,
        swipe: false,
        cssEase: 'linear'
      }).slick('slickPause').on('afterChange', function(event, slick, currentSlide){
      });

      $('#basket a.saddle.active').on('click touch',function(){
        if(!$(this).hasClass('open')){
          $(this).next().show();
          $(this).addClass('open');
        }else{
          $(this).next().hide();
          $(this).removeClass('open');
        }
      });

      $('#basket div.colors ul a.saddle').on('click touch',function(){
        $('div.colors li.active').removeClass('active');
        var i = parseInt($(this).data('index'));
        var price = parseFloat($(this).data('price'));
        var color = parseInt($(this).data('color'));
        $('#basket input[name="color"]').val(color);
        $('#basket .images').slick('slickGoTo', i);
        $(this).parent().addClass('active');
        $('#basket a.saddle.active').html($(this).html()+' <strong>v</strong>');
        $(this).parent().parent().hide();
        $(this).parent().parent().parent().find('a.saddle.active').removeClass('open');

        var number = $('#basket input[name="quantity"]').val();
        $('#basket p.price').data('price',price);
        $('#basket p.price').html(price.toFixed(2)+'&euro;');
        var subtotal = price*number;
        $('#basket p.subtotal').html(subtotal.toFixed(2)+'&euro;');

      });

      var quantity = 0;

      $('#basket input[name="quantity"]').on('change keyup',function(){

        var myString = $('#basket input[name="quantity"]').val();
        $('#basket input[name="quantity"]').val(myString.replace(/[^\d]/g, '')); // 1238

        if($('#basket input[name="quantity"]').val() < 1){
          $('#basket input[name="quantity"]').val(1);
        }
        var number = $('#basket input[name="quantity"]').val();
        var subtotal = parseFloat($('#basket p.price').data('price'));
        subtotal = subtotal*number;
        $('#basket p.subtotal').html(subtotal.toFixed(2)+'&euro;');
      });

      $(window).resize(function(){setTimeout(function(){ centerItems(); }, 500);});

      setTimeout(function(){ centerItems(); }, 500);

      $('a.buy').on('click touch',function(){
        $('#basket div.colors li.active').removeClass('active');
        var i = parseInt($(this).data('index'));
        var color = parseInt($(this).data('color'));

        $('#basket input[name="color"]').val(color);
        $('#basket input[name="quantity"]').val(1);

        $('#basket div.colors li a[data-color="'+color+'"]').parent().addClass('active');
        $('#basket a.saddle.active').html($('#basket div.colors li a[data-color="'+color+'"]').html()+' <strong>v</strong>');
        $('#basket div.colors li a[data-color="'+color+'"]').parent().parent().hide();
        $('#basket').find('a.saddle.active').removeClass('open');
        var price = parseFloat($('#basket div.colors li a[data-color="'+color+'"]').data('price'));
        var number = $('#basket input[name="quantity"]').val();
        $('#basket p.price').data('price',price);
        $('#basket p.price').html(price.toFixed(2)+'&euro;');
        var subtotal = price*number;
        $('#basket p.subtotal').html(subtotal.toFixed(2)+'&euro;');

        $('div.basket__overlay').show();
        $('#basket .images').slick('slickGoTo', i);
      });

      $('a.item').on('click touch',function(){
        var number = $('#basket input[name="quantity"]').val();
        var subtotal = parseFloat($('#basket p.price').data('price'));
       if($(this).hasClass('plus')){
         number++;
         $('#basket input[name="quantity"]').val(number);
         subtotal = subtotal*number;
         $('#basket p.subtotal').html(subtotal.toFixed(2)+'&euro;');
       }else if($(this).hasClass('minus')){
         if(number > 1){
           number--;
           $('#basket input[name="quantity"]').val(number);
           subtotal = subtotal*number;
           $('#basket p.subtotal').html(subtotal.toFixed(2)+'&euro;');
         }
       }
      });

      $('#basket a.cancel').on('click touch',function(){
        $('div.basket__overlay').hide();
      });
      //$('.image').each(function(){
        //var img = $(this).data(dataImgs);
        //$(this).backstretch(img, {fade:'fast',duration: 4000}).backstretch("pause");
      //});

    }

    function total(){
      var total = 0;
      $('form.cart li.body').each(function(){
        var price = parseFloat($(this).find('p.price').data('price'));
        var number = parseFloat($(this).find('input[name="quantity[]"]').val());
        total += price*number;
      });

      $('form.cart').find('p.subtotal').html(total.toFixed(2)+'&euro;');
    }

    if($('body').hasClass('cart')){

      countryIso = $('label.code').data('iso');
      if(countryIso !== null && countryIso !== undefined){
        countryIso = countryIso.toUpperCase();
      }
      region = $('div.input.region').data('code');
      if(countryIso !== "" && countryIso !== null && countryIso !== undefined){
        $('label.code span').text(zipcodes[countryIso][zipcodes[countryIso].length-1]);
        $('label.nif span').text(nifs[countryIso][nifs[countryIso].length-1]);
      }
      if(region == true){
        $('div.input.region').removeClass('hidden');
      }

      $('.images').slick({
        dots: false,
        infinite: true,
        speed: 0,
        fade: true,
        arrows: false,
        adaptiveHeight: true,
        touchMove: false,
        swipe: false,
        cssEase: 'linear'
      }).slick('slickPause').on('afterChange', function(event, slick, currentSlide){
      });

      $('form.cart a.saddle.active').on('click touch',function(){
        if(!$(this).hasClass('open')){
          $('form.cart a.saddle.active.open').next().hide();
          $('form.cart a.saddle.active.open').removeClass('open');
          $(this).next().show();
          $(this).addClass('open');
        }else{
          $(this).next().hide();
          $(this).removeClass('open');
        }
      });

      $('.images').each(function(){
        $(this).slick('slickGoTo', $('div.active',$(this)).data('index'));
      });

      $('form.cart div.colors ul a.saddle').on('click touch',function(){
        $(this).parent().parent().find('li.active').removeClass('active');
        var i = parseInt($(this).data('index'));
        var price = parseFloat($(this).data('price'));
        var color = parseInt($(this).data('color'));
        $(this).parent().parent().parent().parent().find('input[name="color[]"]').val(color);
        $(this).parent().parent().parent().parent().find('.images').slick('slickGoTo', i);
        $(this).parent().addClass('active');
        $(this).parent().parent().parent().parent().find('a.saddle.active').html($(this).html()+' <strong>v</strong>');
        $(this).parent().parent().hide();
        $(this).parent().parent().parent().find('a.saddle.active').removeClass('open');

        $(this).parent().parent().parent().parent().parent().find('p.price').data('price',price);
        $(this).parent().parent().parent().parent().parent().find('p.price').html(price.toFixed(2)+'&euro;');

        total();
        refreshBasket();
      });

      var quantity = 0;

      $('form.cart input[name="quantity[]"]').on('change keyup',function(){

        var myString = $(this).val();
        $(this).val(myString.replace(/[^\d]/g, '')); // 1238

        if($(this).val() < 1){
          $(this).val(1);
        }

        total();
      });

      var currentQuantity = null;

      $('form.cart input[name="quantity[]"]').on('click touch',function(){
        currentQuantity = $(this).val();
        $(this).select();
      });

      $('form.cart input[name="quantity[]"]').on('blur',function(){

        if(currentQuantity !== $(this).val()){

          refreshBasket();

        }

      });

      function refreshBasket(){
        $('form.cart input[name="action"]').val($('form.cart input[type="submit"]').data('basket'));
        $('form.cart').submit();
      }

      $('a.item').on('click touch',function(){
        var number = $(this).parent().find('input[name="quantity[]"]').val();
        if($(this).hasClass('plus')){
          number++;
          $(this).parent().find('input[name="quantity[]"]').val(number);
          total();
          refreshBasket();
        }else if($(this).hasClass('minus')){
          if(number > 1){
            number--;
            $(this).parent().find('input[name="quantity[]"]').val(number);
            total();
            refreshBasket();
          }
        }else if($(this).hasClass('delete')){
          $(this).parent().parent().remove();
          total();
          refreshBasket();
        }
      });

      $('form.cart a.cancel').on('click touch',function(){
        $('form.cart').attr('action',$(this).data('action'));
        $('form.cart').submit();
      });

      var myAlert;
      var myAlertCount=0;

      function myAlertFunction() {
        myAlert = setInterval(alertFunc, 300);
      }

      function alertFunc() {
        if(myAlertCount>3){
          $('p.alert').addClass('highlight');
          clearInterval(myAlert);
          myAlertCount=0;
        }else{
          $('p.alert').toggleClass('highlight');
        }
        myAlertCount++;
      }

      $('form.cart input[type="submit"]').on('click touch',function(e){
        e.preventDefault();
        if($('input[name="deliveryaddress"]:checked').val() == 2){
          if(validateRecuit("#checkoutForm")){
            $('form.cart').attr('action',$(this).data('action'));
            $('form.cart').submit();
          }
        }else{
          if($('input[name="incomaddress"]').val() !== '1'){
            $('form.cart').attr('action',$(this).data('action'));
            $('form.cart').submit();
          }else{
            clearInterval(myAlert);
            myAlertFunction();
          }
        }

      });

      $('div.bottom a.login').on('click touch',function(){
        $('div.top form#loginForm').show();
        $('div.top a.login').addClass('active');
        $('form#loginForm input[name="username"]').focus();
      });


      $('a.country').click(function(){
        $('ul.countries').show();
      });

      $(' ul.countries a').click(function(){
        $(' ul.countries a').removeClass('active');
        $('ul.countries ').hide();
        $('a.country').text($(this).data('title'));
        $('input[name="country"]').val($(this).data('value'));
        countryIso = $(this).data('iso').toUpperCase();
        region = $(this).data('code');
        $('label.code span').text(zipcodes[countryIso][zipcodes[countryIso].length-1]);
        $('label.nif span').text(nifs[countryIso][nifs[countryIso].length-1]);
        if(region == true){
          $('div.region.input').removeClass('hidden');
        }else{
          $('div.region.input').addClass('hidden');
        }
        $(this).addClass('active');
      });


      $('div.radio div.radio').on('click touch',function(){
        $('div.radio div.radio input').prop( "checked", false);
        $("input",$(this)).prop( "checked", true );
        var price = parseFloat($("input",$(this)).data('price'));
        if($("input",$(this)).val()==2){
          $('div.other').show();
          $('#checkoutForm input[type="submit"]').removeClass('disabled');
          $('p.alert').removeClass('highlight');
        }else{
          $('.error').removeClass('error');
          $('div.other').hide();
          if($('p.alert').text() !== undefined && $('p.alert').text() !== null && $('p.alert').text() !== '' ){
            $('#checkoutForm input[type="submit"]').addClass('disabled');
          }
        }
        $('p.type').html(price.toFixed(2)+'&euro;');
      });

    }

    if($('body').hasClass('cartwo')){

      var quantity = 0;

      $('form.cart a.cancel').on('click touch',function(){
        $('form.cart input[name="action"]').val($(this).data('action'));
        $('form.cart').submit();
      });


      $('a.removeCode').on('click touch',function(){
        $('form.cart input[name="action"]').val($(this).data('action'));
        $('form.cart').submit();
      });

      $('form.cart input[type="submit"]').on('click touch',function(e){
        e.preventDefault();
        if($('input[name="deliveryaddress"]:checked').val() == 2){
          if(validateRecuit("#checkoutForm")){
            $('form.cart input[name="action"]').val($(this).data('action'));
            $('form.cart').submit();
          }
        }else{
          if($('input[name="incomaddress"]').val() !== '1'){
            $('form.cart input[name="action"]').val($(this).data('action'));
            if($(this).data('paypal') !== undefined && $(this).data('paypal') !== null && $(this).data('paypal')){
              $('div.paypal__overlay').show();
            }
            $('form.cart').submit();
          }
        }

      });

      $('div.bottom a.login').on('click touch',function(){
        $('div.top form#loginForm').show();
        $('div.top a.login').addClass('active');
        $('form#loginForm input[name="username"]').focus();
      });

      $('a.apply').on('click touch',function(){
        $('input[name="code"]').val($('input[name="promocode"]').val());
        if($('input[name="promocode"]').val() == ""){
          $('input[name="promocode"]').addClass('error');
        }else{
          if($('input[name="codes"]').val() == '1'){
            if(!confirm($('input[name="codeConf"]').val())){
              return false;
            }
          }
          $('input[name="promocode"]').removeClass('error');
          $('#codeForm').submit();
        }
      });

      $('input[name="promocode"]').keypress(function(e){
        if ( e.which == 13 ) return false;
        //or...
        if ( e.which == 13 ) e.preventDefault();
      });

      $('div.radio div.radio').on('click touch',function(){
        $('div.radio div.radio input').prop( "checked", false);
        $("input",$(this)).prop( "checked", true );
        var price = parseFloat($("input",$(this)).data('price'));
        if($("input",$(this)).val()==2){
          $('div.other').show();
        }else{
          $('.error').removeClass('error');
          $('div.other').hide();
        }
        $('p.type').html(price.toFixed(2)+'&euro;');
        $('p.total').html((price+parseFloat($('p.subtotal.price').data('subtotal'))).toFixed(2)+'&euro;');

      });

    }

  });
})();
